data.raw["assembling-machine"]["assembling-machine-1"].ingredient_count = 3

data:extend(
{
  {  type = "item",
    name = "lazzy_roboport",
    icon = "__boblogistics__/graphics/icons/roboport-2.png",
    icon_size = 32,
    flags = {"goes-to-quickbar"},
    subgroup = "bob-logistic-roboport",
    order = "c[signal]-a[roboport-2]",
    place_result = "lazzy_roboport",
    stack_size = 10
},
  {
    type = "roboport",
    name = "lazzy_roboport",
    icon = "__boblogistics__/graphics/icons/roboport-2.png",
    icon_size = 32,
    destructible = false,
    mining_target = nil,
    flags = {"placeable-player", "player-creation"},
    mineable_properties = {minable = false},
    fast_replaceable_group = "roboport",
    max_health = 1000,
    corpse = "big-remnants",
    collision_box = {{-1.7, -1.7}, {1.7, 1.7}},
    selection_box = {{-2, -2}, {2, 2}},
    resistances =
    {
      {
        type = "fire",
        percent = 60
      },
      {
        type = "impact",
        percent = 30
      }
    },
    energy_source =
    {
      type = "electric",
      usage_priority = "secondary-input",
      input_flow_limit = "10MW",
      buffer_capacity = "200MJ"
    },
    dying_explosion = "medium-explosion",
    recharge_minimum = "40MJ",
    energy_usage = "0kW",
    -- per one charge slot
    charging_energy = "0kW",
    logistics_radius = 1000,
    construction_radius = 1000,
    charge_approach_distance = 5,
    robot_slots_count = 10,
    material_slots_count = 10,
    stationing_offset = {0, 0},
    charging_offsets =
    {
      {-1.5, -0.5}, {1.5, -0.5}, {1.5, 1.5}, {-1.5, 1.5},
    },
    base =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-base-2.png",
      width = 143,
      height = 135,
      shift = {0.5, 0.25}
    },
    base_animation =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-base-animation-2.png",
      priority = "medium",
      width = 42,
      height = 31,
      frame_count = 8,
      animation_speed = 0.5,
      shift = {-0.5315, -1.9375}
    },
    base_patch =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-base-patch-2.png",
      priority = "medium",
      width = 69,
      height = 50,
      frame_count = 1,
      shift = {0.03125, 0.203125}
    },
    door_animation_up =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-door-up-2.png",
      priority = "medium",
      width = 52,
      height = 20,
      frame_count = 16,
      shift = {0.015625, -0.875}
    },
    door_animation_down =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-door-down-2.png",
      priority = "medium",
      width = 52,
      height = 22,
      frame_count = 16,
      shift = {0.015625, -0.21875}
    },
    recharging_animation =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-recharging.png",
      priority = "high",
      width = 37,
      height = 35,
      frame_count = 16,
      scale = 1.5,
      animation_speed = 0.5
    },
    working_sound =
    {
      sound = { filename = "__base__/sound/roboport-working.ogg", volume = 0.6 },
      max_sounds_per_type = 3
    },
    recharging_light = {intensity = 0.4, size = 5},
    request_to_open_door_timeout = 15,
    spawn_and_station_height = 0.33,
    radius_visualisation_picture =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-radius-visualization.png",
      width = 12,
      height = 12
    },
    construction_radius_visualisation_picture =
    {
      filename = "__boblogistics__/graphics/entity/roboport/roboport-construction-radius-visualization.png",
      width = 12,
      height = 12
    },
    open_door_trigger_effect =
    {
      {
        type = "play-sound",
        sound = { filename = "__base__/sound/roboport-door.ogg", volume = 1.2 }
      },
    },
    close_door_trigger_effect =
    {
      {
        type = "play-sound",
        sound = { filename = "__base__/sound/roboport-door.ogg", volume = 0.75 }
      },
    },
    circuit_wire_connection_point = circuit_connector_definitions["roboport"].points,
    circuit_connector_sprites = circuit_connector_definitions["roboport"].sprites,
    circuit_wire_max_distance = 10,
    default_available_logistic_output_signal = {type = "virtual", name = "signal-X"},
    default_total_logistic_output_signal = {type = "virtual", name = "signal-Y"},
    default_available_construction_output_signal = {type = "virtual", name = "signal-Z"},
    default_total_construction_output_signal = {type = "virtual", name = "signal-T"},
  }
}
)