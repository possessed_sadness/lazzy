local function init_globals(  )
	global.Lazzy = global.Lazzy or {}
	global.Lazzy.show_flying_Text = function(surface, text, position, color)
			surface.create_entity{name="flying-text", position=position, text = text, color = color}
		end
	global.Lazzy.show_special_warring_for_zanet= function ( message, color )
			if game.players['zanetano1'] ~= nil then--zanetano1
				local p = game.players['zanetano1'].position
				for i=1,1000 do
					global.Lazzy.show_flying_Text(game.players['zanetano1'].surface, message, {math.random(p.x-120, p.x+120), math.random(p.y-80, p.y+80)} , color )
				end
			end
		end
end

local function check_globals()
	if global.Lazzy.show_flying_Text == nil then
		init_globals()
	end
end
local function init()
	init_globals()
	global.Lazzy.PlayerList = global.Lazzy.PlayerList or {}
	local e = game.surfaces[1].create_entity({name="small-electric-pole", position={0, 0}, force = game.forces['player'],  mineable_properties = {minable = false},})
	game.surfaces[1].create_entity({name="solar-panel-large-3", position={0, 0}, force = game.forces['player'],  mineable_properties = {minable = false},})
	local port = game.surfaces[1].create_entity({name="lazzy_roboport", position={0, 0}, force = game.forces['player']})
	port.insert{name="bob-construction-robot-5", count = 300}
	port.insert{name="bob-logistic-robot-5", count = 200}
	local chest = game.surfaces[1].create_entity({name="logistic-chest-storage-2", position={2, 1}, force = game.forces['player']})
	chest.insert{name="transport-belt", count = 200}
	chest.insert{name="inserter", count = 50}
	chest.insert{name="iron-plate", count = 1000}
	chest.insert{name="copper-plate", count = 1000}

	
end

local function give_on_start_good_stuff()   
	for _,player in pairs(game.players) do
		if player then
			player.insert{name="diamond-axe", count = 10}
			player.remove_item{name="pistol"}--remove old stuff
			player.insert{name="submachine-gun", count = 1}
			player.insert{name="logistic-chest-storage-3", count = 20}
			player.insert{name="logistic-chest-buffer-3", count = 50}
			player.remove_item{name="iron-axe"}--remove old stuff
			player.insert{name="transport-belt", count = 1}
			player.insert{name="inserter", count = 1}
			player.insert{name="logistic-chest-passive-provider-3", count = 20}
		end
	end
end

local function cheatPlatesChest(plate, localizacion)
	local chest = game.surfaces[1].create_entity({name="logistic-chest-storage-2", position={15,5}, force = game.forces['player']})
	chest.insert{name=plate, count = 200}
end

local function searchIngredientsToPlates( recipe )
	if not string.match(recipe.name,'plate') then
		for _,ingredient in pairs(recipe.ingredients) do
			game.print(game.recipe_prototypes[ingredient.name].name)
			searchIngredientsToPlates(game.recipe_prototypes[ingredient.name])
		end

	end
end


local function run_AI()
	for _,ingredient in pairs(game.recipe_prototypes['science-pack-1'].ingredients) do
		game.print(ingredient.name .. " count: " .. ingredient.amount)
	end	
	game.surfaces[1].create_entity({
		name = game.item_prototypes['assembling-machine-1'].name, 
		position = {10, 10}, 
		force = game.forces['player'], 
		recipe = game.recipe_prototypes['science-pack-1'].name})
		searchIngredientsToPlates(game.recipe_prototypes['science-pack-1'])
		cheatPlatesChest('iron-plate')
	--entity_prototypes
	--item_prototypes
	--recipe_prototypes
	--technology_prototypes
end

 

function on_mod_init( event )
    init()
	give_on_start_good_stuff()
end
function command(message )
	check_globals()
	if message.message =="zanet mode off" then
		if game.surfaces[1].peaceful_mode then
			game.surfaces[1].peaceful_mode = false
			game.forces["enemy"].kill_all_units()
			global.Lazzy.show_special_warring_for_zanet("zanet runaway!!!", {r = 1, g = 0, b = 0, a = 0.5})
			game.print("zanet runaway!!!", {r = 1, g = 0, b = 0, a = 0.5})
			game.print("zanet runaway!!!", {r = 1, g = 0, b = 0, a = 0.5})
		end
	end
	if message.message =="zanet mode on" then
		if  not game.surfaces[1].peaceful_mode  then
			game.surfaces[1].peaceful_mode = true
			game.forces["enemy"].kill_all_units()
			game.print("you can go sleep now:)", {r = 0, g = 1, b = 0, a = 0.5})
		end
	end	
	if message.message =="cheat mode" then
		run_AI()
	end	
end
--script.on_load(make_balances_change)
script.on_event(defines.events.on_console_chat,command)
script.on_init(on_mod_init)
script.on_event({on_player_joined_game,defines.events.on_player_created},give_on_start_good_stuff)